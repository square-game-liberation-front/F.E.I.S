<div align=center>
    <img src="images/feis logo black.svg" alt="drawing" width=600px/>
</div>

![FEIS glamshot](https://i.ibb.co/5sj6sZR/feis-glamshot.png)

Jubeat chart editor.

Check [the wiki](https://gitlab.com/Buggyroom/F.E.I.S/-/wikis/home) for instructions
