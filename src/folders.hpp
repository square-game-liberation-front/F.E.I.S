#pragma once

#include <filesystem>

std::filesystem::path choose_assets_folder();
std::filesystem::path choose_settings_folder();